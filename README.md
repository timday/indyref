Indyref
=======

Provoked by a chart here <https://imgur.com/bdq2XVd>, which came to my attention
with an accompanying comment about it being evidence of "I'm Alright Jack" thinking influencing the result.  
Some more coverage of this at <http://www.vox.com/2014/9/19/6550159/the-scottish-vote-was-a-class-war-and-the-rich-won>.

"Correlation is not causation" of course; disposable income may be correlated with all sorts of other things:
educational attainment levels, age, hours of sleep even (see <http://dish.andrewsullivan.com/2014/09/18/sleep-is-for-the-rich/>). 

So, %Yes vs. some other things had to be investigated!
This was made easy by the Scottish neighbourhood statistics site.
Unfortunately while it has many wealth, income and employment statistics, "disposable income" of the original chart isn't one of them (see note on ONS stats below).
Some other fairly random things were tried; the statistics site has many more.

Results
-------

[This big plot](https://bitbucket.org/timday/indyref/downloads/indyref-version002.png).

Note that correlation coefficients given and regression lines plotted are computed from unweighted samples, and the correlation is "r", not "r-squared".
(I would have also plotted weighted-by-electorate lines too but sample weighting functionality isn't in the version of numpy/scipy I'm using, and I'm not sure the equal weighting isn't a more sensible approach anyway).

I won't attempt to analyse the results.  The things I think are *notable* are:

* 'Benefit claimants' as the highest single highest correlated statistic (with DLA claimants not far behind).
* The good correlation of life expectancy.  Of course life expectancy itself is computed taking into account a multitude of factors.
* Compared with the above, the comparative lack of correlation of "obvious" wealth statistics, although the simple 'households with savings' isn't bad.
* The *lack* of correlation with pensioner numbers (despite clear demographic trends in opinion polls).

Sources
-------

Voting statistics from <http://www.cityam.com/1411046935/who-won-where-how-scottish-councils-voted-independence-referendum-results-map>.  Table captured to `sources/votes.txt`.  Tab separated columns are: Council area, Turnout%, Yes%, No%, #Yes, #No.

Scottish neighbourhood statistics site is very good <http://www.sns.gov.uk> for all sorts of stuff (especially the "Special interest reports" section).  Used "advanced reporter" to obtain these tables:

* 2011 Total Population ("mid-year estimates").  In sources/pop2011.txt.  Used to normalize some statistics not given as percentages.

* 2009-2010 % of households describing themselves as coping well or very well financially.  In sources/copingwell.txt.
* 2012 Median gross weekly earnings for full-time employees (residence based).  In sources/earnings.txt.
* 2011Q04 Employment rate (16-64).  In sources/employment.txt.
* 2009-2010: % of households that have some savings.  In sources/savings.txt.
* 2012 VAT/PAYE registrations per 10,000 adults.  In sources/vat.txt

* 2013 % people with degree (16-64).  In sources/degree.txt.
* 2013 % people with low or no quals (16-64).  In sources/lowqual.txt.  NB No data for Orkney?
* 2009-2010: % adults aged 16+ giving up time to volunteer in the previous 12 months.  In sources/volunteer.txt.
* 2009-2010: % of adults who have taken part in a cultural activity in the last 12 months.  In sources/culture.txt.
* 2009-2010 : % adults agreeing with the statement 'I can influence decisions affecting my local area'.  In sources/influence.txt

* 2012-2013: % of households with access to a bicycle.  In sources/bicycle.txt.
* 2009-2010: % of adults who have taken part in a sporting activity in the last 12 months (excluding walking) 
* 2012 % smoking.  In sources/smokers.txt.
* 2011/2012  Alcohol Related Hospital Discharge Rate per 100,000.  In sources/alcohol.txt.
* 2011 Coronary Heart Disease Admissions - both sexes - all ages - rate/100,000.  In sources/coronary.txt.

* 2013-2013 Total crimes and offences per 10,000 population.  In sources/crime.txt.
* 2011/2012 All priority homeless.  In sources/n_homeless.txt (needs normalization).
* 2012Q04 DLA claimants per 100,000.  In sources/dla.txt.
* 2009Q04 % working age population claiming key benefits.  In sources/benefits.txt.
* 2011Q3 Total State Pension claimants.  In sources/n_pension.txt (needs normalization).

* 2012 % of adults who rate their neighbourhood as a very good place to live.  In sources/vgood.txt.
* 2012 % dwellings in bands F-H.  In sources/bandfh.txt.
* 2012 House sales, mean price.  In sources/housemean.txt.
* 2008-2010 male and female LE (1 plot each).  In sources/lifeexpectancy.txt.

![Correlate all the data!](http://i.imgur.com/xrRhZiO.jpg)

Inbox:

* 2013 % Pupils from ethnic minority groups.  In sources/pupils_ethnic.txt.
* 2007 Free school meals: Percentage primary and secondary pupils.  In sources/freeschoolmeals.txt.
* 2011 No pupils (primary + secondary ).  In sources/pupils.txt (2 columns primary and secondary).
* 2011 Pensioners to pupils ratio.
* 2009 New individuals reported to the Scotttish Drug Misuse Database - both sexes - all ages; rate per 100 000. In sources/drugs.txt.  No Orkney data.
* 2011-2013 Percentage of women smoking at booking (maternity).  In sources/smokers_maternity.txt.

From other places (but not used):

* Education data by region (2010 latest I could find):  <http://www.scotland.gov.uk/Publications/2011/08/09172458/7>.  Table 4.1 "Highest level of qualification held by people aged 16-64 by local authority, Scotland, 2010".  Table captured to `sources/qualifications.txt`.  Tab separated columns are: "Residence", "Degree-level Qualification", "HNC, HND or equivalent", "Higher, A-Level or Equivalent", "Credit Standard grade or Equivalent", "General Standard Grade or Equivalent", "Other Qualifications", "No Qualifications", "Total".  With each qualification level having a % and a number column.

* The ONS site <http://www.ons.gov.uk> has some interesting statistics (including the disposable income one which inspired the project), but it seems to handle regions/local authority areas differently to the SNS (groups some together), so would need some additional data wrangling to make sense of.

Other interesting links
-----------------------

* Influence of information on voting: <http://www.theguardian.com/politics/2014/sep/07/scottish-independence-referendum-research-more-information-likely-vote-yes>, full paper at <http://cadmus.eui.eu/handle/1814/32411> 
* Poll analysing some aspects of voting preference:<http://www.theguardian.com/politics/2014/sep/20/scottish-independence-lord-ashcroft-poll>
* Education and income (in USA anyway): <http://www.huffingtonpost.com/steven-strauss/the-connection-between-ed_b_1066401.html>

