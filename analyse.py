#!/usr/bin/env python

import glob
import matplotlib
matplotlib.use('Agg')    
import matplotlib.pyplot as plt
import numpy as np
import os
import scipy.stats

def readfile(filename,places,reader,scaling=1.0):
    values={}
    for line in file(filename):
        if len(line.strip())>0:
            fields=[f.strip() for f in line.split('\t')]
            where=fields[0]
            where=where.replace('Glasgow City','Glasgow')
            where=where.replace('Edinburgh, City of','Edinburgh')
            where=where.replace('Aberdeen City','Aberdeen')
            where=where.replace('Dundee City','Dundee')
            where=where.replace('Comhairle nan Eilean Siar','Eilean Siar')
            if not where in places:
                print 'Unknown region "{0}" in file "{1}"'.format(where,filename)
            else:
                values[where]=scaling*reader(fields,where)
    if len(values)!=len(places):
        print '{0} region shortfall in file "{1}"'.format(len(places)-len(values),filename)
    return values

def plot(xv,yv,weight,xlabel,ylabel,title):

    places=sorted(xv.keys())

    x=[xv[p] for p in places]
    y=[yv[p] for p in places]
    plt.scatter(x,y)

    slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x, y)

    print '{0}: {1:.3f}'.format(title,r_value)

    rx=np.linspace(np.amin(x),np.amax(x))
    ry=rx*slope+intercept
    plt.plot(rx,ry)

    p=np.polyfit(x,y,deg=2)
    ry=p[0]*rx**2+p[1]*rx+p[2]
    plt.plot(rx,ry)

    rankr_value,p=scipy.stats.spearmanr(x,y)
    
    for i in xrange(len(places)):
        plt.text(x[i],y[i],'  - '+places[i],withdash=True,fontsize=6,verticalalignment='center', horizontalalignment='left')

    if plt.xlim()[0]<0.0:
        plt.xlim(0.0,plt.xlim()[1])

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title+'\nPearson r: {0:.3f}, Spearman (rank) r: {1:.3f}'.format(r_value,rankr_value))

def main():

    turnout={}
    yes={}
    voted={}
    for line in file('sources/votes.txt'):
        fields=[f.strip() for f in line.split('\t')]
        where=fields[0]
        turnout[where]=float(fields[1].replace(',',''))
        yes[where]=float(fields[2].replace(',',''))
        voted[where]=int(fields[4].replace(',',''))+int(fields[5].replace(',',''))

    places=yes.keys()

    getfield1=lambda fields,where: float(fields[1].replace(',',''))
    getfield2=lambda fields,where: float(fields[2].replace(',',''))
    
    population=readfile('sources/pop2011.txt',places,getfield1)

    normfield1=lambda fields,where: getfield1(fields,where)/population[where]

    copingwell=readfile('sources/copingwell.txt',places,getfield1)
    degree=readfile('sources/degree.txt',places,getfield1)
    earnings=readfile('sources/earnings.txt',places,getfield1)
    lifex_male=readfile('sources/lifeexpectancy.txt',places,getfield1)
    lifex_female=readfile('sources/lifeexpectancy.txt',places,getfield2)
    employment=readfile('sources/employment.txt',places,getfield1)
    dla=readfile('sources/dla.txt',places,getfield1)
    crime=readfile('sources/crime.txt',places,getfield1)
    bandfh=readfile('sources/bandfh.txt',places,getfield1)
    bicycle=readfile('sources/bicycle.txt',places,getfield1)
    culture=readfile('sources/culture.txt',places,getfield1)
    housemean=readfile('sources/housemean.txt',places,getfield1,1.0/1000.0)
    lowqual=readfile('sources/lowqual.txt',places,getfield1)
    savings=readfile('sources/savings.txt',places,getfield1)
    sport=readfile('sources/sport.txt',places,getfield1)
    volunteer=readfile('sources/volunteer.txt',places,getfield1)
    vat=readfile('sources/vat.txt',places,getfield1)
    influence=readfile('sources/influence.txt',places,getfield1)
    vgood=readfile('sources/vgood.txt',places,getfield1)
    benefits=readfile('sources/benefits.txt',places,getfield1)
    homeless=readfile('sources/n_homeless.txt',places,normfield1,100000.0)
    pension=readfile('sources/n_pension.txt',places,normfield1,100.0)
    smokers=readfile('sources/smokers.txt',places,getfield1)
    alcohol=readfile('sources/alcohol.txt',places,getfield1)
    coronary=readfile('sources/coronary.txt',places,getfield1)

    normfields12sum=lambda fields,where: (getfield1(fields,where)+getfield2(fields,where))/population[where]

    pupils=readfile('sources/pupils.txt',places,normfields12sum,100.0)
    pupils_ethnic=readfile('sources/pupils_ethnic.txt',places,getfield1)
    freeschoolmeals=readfile('sources/freeschoolmeals.txt',places,getfield1)

    pensioners_per_pupil={p:pension[p]/pupils[p] for p in places}

    drugs=readfile('sources/drugs.txt',places,getfield1)
    smokers_maternity=readfile('sources/smokers_maternity.txt',places,getfield1)

    plt.figure(figsize=(48,48))
    plt.suptitle('''Correlating regions' Yes vote with data from "Scottish neighbourhood statistics"\n(http://www.sns.gov.uk)\nReminder: correlation is not causation!''',fontsize=48)
    plt.subplots_adjust(hspace=0.4)

    rows=6
    cols=6

    plt.subplot(rows,cols,1)
    plot(
        vat,
        yes,
        voted,
        'VAT/PAYE registrations per 10,000 adults 2012',
        '% Yes',
        '''The Entrepeneurial Vote'''
        )

    plt.subplot(rows,cols,2)
    plot(
        earnings,
        yes,
        voted,
        'Median gross weekly earnings for full-time employees 2012',
        '% Yes',
        '''The "I'm Alright Jack" Vote'''
        )

    plt.subplot(rows,cols,3)
    plot(
        copingwell,
        yes,
        voted,
        '% of households describing themselves as coping\nwell or very well financially 2009-2010',
        '% Yes',
        '''The "We're Alright Jack" Vote'''
        )

    plt.subplot(rows,cols,4)
    plot(
        employment,
        yes,
        voted,
        '% Employment rate (16-64) 2011Q04',
        '% Yes',
        '''The Employed Vote'''
        )

    plt.subplot(rows,cols,5)
    plot(
        savings,
        yes,
        voted,
        '% of households that have some savings 2009-2010',
        '% Yes',
        '''The "Money In The Bank" Vote'''
        )

    plt.subplot(rows,cols,6)
    plot(
        degree,
        yes,
        voted,
        '% people with degree (16-64) 2013',
        '% Yes',
        '''The Smart Vote'''
        )

    plt.subplot(rows,cols,7)
    plot(
        culture,
        yes,
        voted,
        '% of adults who have taken part in a cultural \nactivity in the last 12 months 2009-2010',
        '% Yes',
        '''The Cultured Vote'''
        )

    plt.subplot(rows,cols,8)
    plot(
        influence,
        yes,
        voted,
        '% adults agreeing with the statement "I can influence\ndecisions affecting my local area" 2009-2010',
        '% Yes',
        '''The Engaged Vote'''
        )

    plt.subplot(rows,cols,9)
    plot(
        volunteer,
        yes,
        voted,
        '% adults aged 16+ giving up time to volunteer\nin the previous 12 months 2009-2010',
        '% Yes',
        '''The Volunteer Vote'''
        )

    plt.subplot(rows,cols,10)
    plot(
        lowqual,
        yes,
        voted,
        '% people with low or no quals (16-64) 2013',
        '% Yes',
        '''The Left Behind Vote'''
        )

    plt.subplot(rows,cols,11)
    plot(
        coronary,
        yes,
        voted,
        ' Coronary Heart Disease\nAdmissions rate per 100,000 2011',
        '% Yes',
        '''The Heart Attack Vote'''
        )

    plt.subplot(rows,cols,12)
    plot(
        alcohol,
        yes,
        voted,
        'Alcohol Related Hospital\nDischarge Rate per 100,000 2011/2012',
        '% Yes',
        '''The Buckfast Vote'''
        )

    plt.subplot(rows,cols,13)
    plot(
        sport,
        yes,
        voted,
        '% of adults who have taken part in a sporting activity\nin the last 12 months (excluding walking) 2009-2010',
        '% Yes',
        '''The Sporty Vote'''
        )

    plt.subplot(rows,cols,14)
    plot(
        smokers,
        yes,
        voted,
        '% smoking 2012',
        '% Yes',
        '''The Nicotine Vote'''
        )

    plt.subplot(rows,cols,15)
    plot(
        bicycle,
        yes,
        voted,
        '% of households with access to a bicycle 2012-2013',
        '% Yes',
        '''The Cyclist Vote'''
        )

    plt.subplot(rows,cols,16)
    plot(
        pension,
        yes,
        voted,
        '% State Pension claimants 20011Q3',
        '% Yes',
        '''The Pensioner Vote'''
        )

    plt.subplot(rows,cols,17)
    plot(
        crime,
        yes,
        voted,
        'Total crimes and offences per 10,000 population 2012-2013',
        '% Yes',
        '''The "Bad Area" Vote'''
        )

    plt.subplot(rows,cols,18)
    plot(
        homeless,
        yes,
        voted,
        'All priority homeless per 100,000',
        '% Yes',
        '''The "Out On The Streets" Vote'''
        )

    plt.subplot(rows,cols,19)
    plot(
        dla,
        yes,
        voted,
        'DLA claimants per 100,000 2012Q04',
        '% Yes',
        '''The Disabled Vote'''
        )

    plt.subplot(rows,cols,20)
    plot(
        benefits,
        yes,
        voted,
        '% Working age population claiming key benefits 2009Q4',
        '% Yes',
        '''The Benefits Vote'''
        )

    plt.subplot(rows,cols,21)
    plot(
        bandfh,
        yes,
        voted,
        '% dwellings in bands F-H 2012',
        '% Yes',
        '''The Mansions Vote'''
        )

    plt.subplot(rows,cols,22)
    plot(
        housemean,
        yes,
        voted,
        'House sales, mean price (thousands) 2012',
        '% Yes',
        '''The Mortgaged Vote'''
        )

    plt.subplot(rows,cols,23)
    plot(
        vgood,
        yes,
        voted,
        '% of adults who rate their neighbourhood\nas a very good place to live 2012',
        '% Yes',
        '''The Nice Area Vote'''
        )

    plt.subplot(rows,cols,24)
    plot(
        lifex_female,
        yes,
        voted,
        'Female life expectancy 2008-2010',
        '% Yes',
        '''The "In Her Lifetime?" Vote'''
        )

    plt.subplot(rows,cols,25)
    plot(
        lifex_male,
        yes,
        voted,
        'Male life expectancy 2008-2010',
        '% Yes',
        '''The "In His Lifetime?" Vote'''
        )

    plt.subplot(rows,cols,26)
    plot(
        pupils_ethnic,
        yes,
        voted,
        '% Pupils from ethnic minority groups 2013',
        '% Yes',
        '''The Rainbow Vote'''
        )

    plt.subplot(rows,cols,27)
    plot(
        freeschoolmeals,
        yes,
        voted,
        '% Free school meals (primary and secondary) 2007',
        '% Yes',
        '''The Square Meal Vote'''
        )

    plt.subplot(rows,cols,28)
    plot(
        pupils,
        yes,
        voted,
        '% Pupils (primary and secondary) 2011',
        '% Yes',
        '''The Youthful Vote'''
        )

    plt.subplot(rows,cols,29)
    plot(
        pensioners_per_pupil,
        yes,
        voted,
        'Pensioners per pupil 2011',
        '% Yes',
        '''The Demographic Vote'''
        )

    plt.subplot(rows,cols,30)
    plot(
        drugs,
        yes,
        voted,
        'New individuals reported to the Scottish Drug\nMisuse Database (per 100,000) 2009',
        '% Yes',
        '''The Junkie Vote'''
        )

    plt.subplot(rows,cols,31)
    plot(
        smokers_maternity,
        yes,
        voted,
        'Percentage of women smoking at maternity booking 2011-2013',
        '% Yes',
        '''The Nicotine Baby Vote'''
        )

    plt.savefig('indyref.png')

    print 
    print 'Analysing primary influences:'

    # Rest of this attempts to cook up a 'the four numbers which determine indyref outcome revealed' type story,
    # but once the influence of the most high correlation factor (% on benefits) is removed there's nothing emerges
    # which really drives down the remaining errors much.
    # Better to do some proper principal components analysis?
    # Probably just end up with a big crazy overfitted expression though.

    variables={
        'copingwell':copingwell,
        'degree':degree,
        'earnings':earnings,
        'lifex_male':lifex_male,
        'lifex_female':lifex_male,
        'employment':employment,
        'dla':dla,
        'crime':crime,
        'bandfh':bandfh,
        'bicycle':bicycle,
        'culture':culture,
        'housemean':housemean,
        'savings':savings,
        'sport':sport,
        'volunteer':volunteer,
        'vat':vat,
        'influence':influence,
        'vgood':vgood,
        'benefits':benefits,
        'homeless':homeless,
        'pension':pension,
        'smokers':smokers,
        'alcohol':alcohol,
        'coronary':coronary,
        'pupils':pupils,
        'pupils_ethnic':pupils_ethnic,
        'freeschoolmeals':freeschoolmeals,
        'pensioners_per_pupil':pensioners_per_pupil,
        'smokers_maternity':smokers_maternity
        } # Excludes lowqual and drugs; no data for Orkney causes trouble

    def correlate(vname,tgt):
        x=[variables[vname][p] for p in places]
        return scipy.stats.linregress(x,tgt)
    
    tgt=np.array([yes[p] for p in places])

    # 1st correction:
    result=sorted([(vname,correlate(vname,tgt)) for vname in variables.keys()],key=lambda x: x[1][2],reverse=True)
    print result[0]

    bname=result[0][0]
    slope=result[0][1][0]
    intercept=result[0][1][1]

    tgt=tgt-np.array([intercept+slope*variables[bname][p] for p in places])

    # 2nd correction:

    result=sorted([(vname,correlate(vname,tgt)) for vname in variables.keys()],key=lambda x: x[1][2],reverse=True)
    print result[0]
                                                    
    bname=result[0][0]
    slope=result[0][1][0]
    intercept=result[0][1][1]

    tgt=tgt-np.array([intercept+slope*variables[bname][p] for p in places])

    # 3rd correction:

    result=sorted([(vname,correlate(vname,tgt)) for vname in variables.keys()],key=lambda x: x[1][2],reverse=True)
    print result[0]

    bname=result[0][0]
    slope=result[0][1][0]
    intercept=result[0][1][1]

    tgt=tgt-np.array([intercept+slope*variables[bname][p] for p in places])

    # 4th correction:

    result=sorted([(vname,correlate(vname,tgt)) for vname in variables.keys()],key=lambda x: x[1][2],reverse=True)
    print result[0]

    bname=result[0][0]
    slope=result[0][1][0]
    intercept=result[0][1][1]

    tgt=tgt-np.array([intercept+slope*variables[bname][p] for p in places])

    # What's left?

    print
    print 'Residuals:'
    for i in range(len(places)):
        print places[i],tgt[i]
        
if __name__ == '__main__':
    main()
