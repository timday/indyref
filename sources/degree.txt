Aberdeen City	31.8
Aberdeenshire	24.7
Angus	19.4
Argyll and Bute	23.7
Clackmannanshire	20.2
Comhairle nan Eilean Siar	23.2
Dumfries and Galloway	17.2
Dundee City	26.9
East Ayrshire	16.2
East Dunbartonshire	37.6
East Lothian	27.0
East Renfrewshire	32.0
Edinburgh, City of	42.0
Falkirk	16.1
Fife	22.3
Glasgow City	30.7
Highland	21.0
Inverclyde	19.0
Midlothian	18.4
Moray	17.2
North Ayrshire	12.3
North Lanarkshire	15.3
Orkney Islands	15.8
Perth and Kinross	25.2
Renfrewshire	25.6
Scottish Borders	23.8
Shetland Islands	18.4
South Ayrshire	20.5
South Lanarkshire	18.4
Stirling	29.5
West Dunbartonshire	14.1
West Lothian	20.0
