Aberdeen City	8.8
Aberdeenshire	10.7
Angus	14.7
Argyll and Bute	9.6
Clackmannanshire	14.4
Comhairle nan Eilean Siar	9.6
Dumfries and Galloway	15.6
Dundee City	15.5
East Ayrshire	18.0
East Dunbartonshire	6.8
East Lothian	10.5
East Renfrewshire	7.4
Edinburgh, City of	6.7
Falkirk	13.3
Fife	9.2
Glasgow City	18.4
Highland	9.9
Inverclyde	15.5
Midlothian	14.6
Moray	10.8
North Ayrshire	17.0
North Lanarkshire	17.5
Perth and Kinross	10.5
Renfrewshire	13.7
Scottish Borders	10.6
Shetland Islands	10.9
South Ayrshire	11.2
South Lanarkshire	13.1
Stirling	11.6
West Dunbartonshire	18.6
West Lothian	11.4
