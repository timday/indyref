Aberdeen City	11735	8752
Aberdeenshire	19088	15054
Angus	8323	6824
Argyll and Bute	5725	5307
Clackmannanshire	3759	2999
Comhairle nan Eilean Siar	1860	1674
Dumfries and Galloway	10301	8930
Dundee City	9557	7384
East Ayrshire	8807	7332
East Dunbartonshire	7280	6628
East Lothian	7599	5978
East Renfrewshire	7255	6398
Edinburgh, City of	24851	18244
Falkirk	11613	9314
Fife	26729	20899
Glasgow City	37538	29336
Highland	16410	14347
Inverclyde	5412	4722
Midlothian	6248	5102
Moray	6153	5665
North Ayrshire	10100	8369
North Lanarkshire	26653	21585
Orkney Islands	1354	1284
Perth and Kinross	9665	7710
Renfrewshire	12342	10356
Scottish Borders	8045	6661
Shetland Islands	1783	1506
South Ayrshire	7359	6483
South Lanarkshire	23265	18986
Stirling	6383	5404
West Dunbartonshire	6600	5478
West Lothian	14430	11280
